﻿using Academy.Entities.Identity;
using IdentityServer4.EntityFramework.Options;
using Microsoft.AspNetCore.ApiAuthorization.IdentityServer;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using System;
using System.Linq;

namespace Academy.AppContext
{



    public class AcademyDbContext : ApiAuthorizationDbContext<ApplicationUser>
    {
       
        public AcademyDbContext(
           DbContextOptions options,
           IOptions<OperationalStoreOptions> operationalStoreOptions) : base(options, operationalStoreOptions)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {   
            base.OnModelCreating(modelBuilder);
        }
        #region IdentityArea
        #endregion

        #region MyAgency
        #endregion
 
    }
}
